# FreeNAS Active-Passive Storage

At Yarra Valley FM, we are using [FreeNAS](https://www.freenas.org/) to run our
primary broadcast storage. This storage is our source-of-truth for all our
audio assets:

* Music Library, including Playlists
* Sponsorships and Community Service Announcements
* Sweepers, Stings, Show Promos
* Production Assets (eg, projects, stems etc)
* Syndicated Programs
* Archived audio assets
* Broadcast Audio Logs

Since we broadcast 24x7, this storage needs to have near 24x7 availability, but
we're a communtiy radio station and we don't have the budget for a "true" [Five
Nines](https://en.wikipedia.org/wiki/High_availability#Percentage_calculation)
storage system.

This repository is documentation of our building a super-low (relatively) budget
active-passive storage system using FreeNAS.

**NOTE: THIS DOCUMENT IS A WORK-IN-PROGRESS STARTING FEB 2020.** Updates will be
made as the project progresses.

## Goals

1. Physically redundant storage using *warm standby* host.
1. Usable by current clients (ie, Windows via SMB, Linux servers via NFS)
1. Manual fail-over is acceptable.
1. Continue using ZFS for data security (and all the other reasons [ZFS is better](http://whyzfsisbetter.com/)).

## Components

We started with 2 identical hardware boxes, each built from the following parts
for a total cost of less than AUD$1,000, excluding storage devices.

| Part                                             |  Cost  |
| ---                                              | ------:|
| SilverStone DS380 Black NAS Case                 | `$209` |
| ASUS Prime H310I-PLUS Motherboard                | `$129` |
| Intel Core i3 8100                               | `$199` |
| Crucial CT16G4DFD824A 16GB (1x16GB) 2400MHz DDR4 |  `$95` |
| Intel 545s Series 128GB M.2 SSD                  |  `$65` |
| StarTech PEXSAT34RH SATA HBA Controller          | `$149` |
| SilverStone 500W SFX Power Supply                | `$129` |
| **TOTAL COST PER BOX:**                        | **`$975`** |

*All pricing above is in AUD in Feb 2020*

Despite the availability requirements of this storage, it is not actually
heavily used; we have a single active playout PC reading music and other
assets in real-time as it plays them, and occasional access from our production
studio. The CPU is an entry level i3 with 4-cores, but clocks in at 3.60Ghtz
which should be ample for these machines, even with ZFS compression.

Each box is starting with 16GB RAM; double the current system that this
project will replace, and easy to expand in future if needed. We do not use
ZFS deduplication, so we don't need to allow RAM for that, and with only ~12tb
of raw storage, 16gb RAM should be ample.

The SilverStone DS380 is a purpose-build NAS box for Mini-ITX motherboards,
supporting up to 8 hot-swap 3.5" or 2.5" hard-disks, and 4 internal 2.5" disks.
And at only 211mm wide, they should be able to sit side-by-side on a shelf in
a standard 19" rack also.

An Intel 128gb M.2 SSD for the FreeNAS boot drive, and a StarTech SATA expansion
card to provide connectivity for the extra drive bays that the case supports
completes the build. Important to note is that the M.2 drive in SATA mode disables
the on-board `SATA_2` port on the motherboard, so only 3 of the 4 on-board SATA
ports will be available for use. In total, we will be able to have 7 storage disks
with this hardware (3 on-board + 4 from the Startech controller)

### Hard disks?

We are fortunate to have a surplus of spinning-rust hard-disks to be used for
this project, so we don't have to include that in the cost. For the purposes of
documenting the project though I think this is good - storage systems can vary
drastically depending on the amount of storage being provisioned, while the
underlying host platform remains relatively constant. For example, provisioning
this system with 96tb of raw storage using 8× 12tb Western Digital Red Pro
disks would add a whopping $6,152 to the cost, while a smaller setup with just
2× 2tb standard WD Red disks would only add an extra $238.

For our purposes, we will be provisioning the system with 4× 3tb WD Red disks
in 2× mirrors. This gives us sufficient storage for now (~6tb), and the option
to add another 2 or 4 mirrored disks to the unpopulated hot-swap bays for future
expansion.
